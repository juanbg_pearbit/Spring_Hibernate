package io.pearbit.exampleSpring.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import javax.persistence.JoinColumn;

@Component
@Entity //Declara como entidad
@Table(name = "users") // nombre de la tabla fisica
public class User {
	
	@Id 
	@Column(name = "user_id") //nombre de la columna
	@GeneratedValue //auto_increment
	private int id;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "enable_status")
	private int enable;	
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL}) //relacion de muchos a muchos 	
	@JoinTable(name="user_role",  //nombre de la tabla relacional
				joinColumns={@JoinColumn(name="user_id")}, //tabla por dponde comienza relación 
				inverseJoinColumns={@JoinColumn(name="role_id")}) //otra entidad de relación 
	private List<Rol> rol;
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}	
	
	
	
	
	public User(int id, String username, String password, int enable, List<Rol> rol) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.enable = enable;
		this.rol = rol;
	}




	public List<Rol> getRol() {
		return rol;
	}




	public void setRol(List<Rol> rol) {
		this.rol = rol;
	}




	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int isEnable() {
		return enable;
	}
	public void setEnable(int enable) {
		this.enable = enable;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", enable=" + enable + ", rol="
				+ rol + "]";
	}

	
	
	
	
}
