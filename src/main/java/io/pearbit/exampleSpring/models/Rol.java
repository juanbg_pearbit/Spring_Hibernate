package io.pearbit.exampleSpring.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity //declara como entidad
@Table(name = "role") //nombre de tabla física
public class Rol {
	
	@Id //id
	@GeneratedValue //auto_increment
	@Column(name = "role_id")
	private int id;
	@Column (name = "role_name")
	private String name;
	@ManyToMany(mappedBy="rol")//es el atributo rol en clase User
	private List<User> user;
	
	public Rol(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public Rol() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<User> getUser() {
		return user;
	}
	public void setUser(List<User> user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Rol [id=" + id + ", name=" + name + "]";
	}
	
	
	
	
	
	
	 
}
