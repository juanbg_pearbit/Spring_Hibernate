package io.pearbit.exampleSpring.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import io.pearbit.exampleSpring.utils.DummyInjection;
/**
 * This class is the custom configuration for our spring project. It depends about we get spring started, either xml via or java.
 * in this case Spring is started in SpringWebConfig.java.  
 *
 * For make the work of make a custom configuration we extended of {@link WebMvcConfigurerAdapter} it allows us only override methods which we need.
 * 
 * Annotations used here are in order for: 
 * {@link Configuration} enable this class to be a configuration file
 * {@link EnableWebMvc} specify that this project will use the WebMVC configuration
 * {@link ComponentScan} allows us scan the mvc components in our class path 
 * {@link PropertySource} it give us the ability to read external files for example, properties files. 
 * {@link Import} this annotations import another configuration classes, in this project will be used to load Security and Session configuration classes.
 * 
 * @author JuanBG
 * @see SpringWebInitializer
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"io.pearbit.exampleSpring"})
@PropertySource("META-INF/application.properties")
public class SpringWebConfig extends WebMvcConfigurerAdapter{
	
	@Value("${database.driver}") private String dbDriver;
	@Value("${database.url}") private String dbUrl;
	@Value("${database.user}") private String dbUser;
	@Value("${database.password}") private String dbPassword;

	// values for hibernate
	@Value("${hibernate.dialect}") private String hdialect;
	@Value("${hibernate.show_sql}") private String hshowSql;
	@Value("${hibernate.format_sql}") private String hFormatSql;
	@Value("${hibernate.hbm2ddl.auto}") private String ddl_auto;
	
	/**
	 * (non-Javadoc)
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#addResourceHandlers(org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry)
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").
		addResourceLocations("/WEB-INF/resources/");
	}
	
	/**
	 * {@inheritDoc}
	 * @return the view gave by the resolver
	 */
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/resources/views/");
		resolver.setSuffix(".jsp");
		resolver.setExposeContextBeansAsAttributes(true);
		return resolver;
	}
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean(); //Create a new object for be returned like in a Singleton 
		sessionFactory.setDataSource(restDataSource()); //Consumes the datasoruce with the database information
		sessionFactory.setPackagesToScan(new String[] { "io.pearbit.exampleSpring.models" }); //points out where are the entities  
		sessionFactory.setHibernateProperties(hibernateProperties()); //Are sent the hibernate props. 
		return sessionFactory;
	}
	@Bean
	public DataSource restDataSource() {
		BasicDataSource dataSource = new BasicDataSource(); //are sent all props read from the config file
		dataSource.setDriverClassName(dbDriver);
		dataSource.setUrl(dbUrl);
		dataSource.setUsername(dbUser);
		dataSource.setPassword(dbPassword);
		// dataSource.setPassword(env.getProperty("jdbc.pass"));
		
		return dataSource;
	}
	
	Properties hibernateProperties() {
	return new Properties() {
		{
			setProperty("hibernate.dialect", hdialect);
			setProperty("hibernate.format_sql", hFormatSql);
			setProperty("hibernate.show_sql", hshowSql);
			setProperty("hibernate.hbm2ddl.auto", ddl_auto);

		}
	};
}
	
}
		