package io.pearbit.exampleSpring.dao.services;

import java.sql.SQLException;
import java.util.List;

/**
 * Interface que deberá ser implementado en todos los cruds 
 * @author JuanBG
 *
 * @param <T>
 */
public interface CrudImpl <T>{

	// Metodo generico que permite insertar
	public String insertar(T t) throws SQLException;

	// Metodo generico que permite editar
	public String editar(T t) throws SQLException;
	
	//Metodo generico que permite eliminar
	public String eliminar(int t )throws SQLException;
	
	//Metodo generico que permite consultar general 
	public List<T> consultaGeneral() throws SQLException;
	
	//Metodo generico que permite consultar de forma individual
	public T consultaIndividual(String t) throws SQLException;
	
}