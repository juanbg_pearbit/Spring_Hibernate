package io.pearbit.exampleSpring.dao.sql;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import io.pearbit.exampleSpring.dao.services.CrudImpl;
import io.pearbit.exampleSpring.models.Rol;
import io.pearbit.exampleSpring.models.User;

@Repository
public class RolDAO implements CrudImpl<Rol>{

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public String insertar(Rol t) throws SQLException {
		Session session = sessionFactory.openSession(); //abrir sesión
		try {
			session.beginTransaction(); // Comenzar transacción, todas las operaciones que tocan la base de datos, se les conoce como transacciones.
			session.save(t); //una vez abierta la sesión le decimos a hibernate que guarde en el área de espera el recurso a persistir 
			session.getTransaction().commit();//le damos la orden a la transacción de realizar el commit a la base de datos 

			return "Exito al insertar"; //retornamos estatus
		} catch (Exception e) {
			session.getTransaction().rollback(); //si algo falla hacemos un rollback para deshacer los posibles cambios
			e.printStackTrace();
			throw new SQLException("Existe un problema con la base de datos \n" + "Error al insertar.");
		} finally {
			session.close();//cerramos sesión

		}

	}

	@Override
	public String editar(Rol t) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String eliminar(int t) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Rol> consultaGeneral() throws SQLException {
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			Query query = session.createQuery("from Rol"); //HQL que hace referencia a un SELECT * FROM users, sólo que esta hace una referencia al Objeto mapeado en el hibernate.cgf.xml.
			List<Rol> roles = query.list(); //obtenemos una lista 
			session.getTransaction().commit();
			return roles;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SQLException("Error al consultar: " + e.getMessage());
		} finally {
			session.close();
		}

	}

	@Override
	public Rol consultaIndividual(String t) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
