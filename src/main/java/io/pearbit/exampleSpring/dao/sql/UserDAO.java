package io.pearbit.exampleSpring.dao.sql;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import io.pearbit.exampleSpring.dao.services.CrudImpl;
import io.pearbit.exampleSpring.models.User;


/**
 * Clase de persistencia a la base de datos, se basa en un DAO del objeto Usuario
 * Es importante notar que para cada metodo es importante abrir sesión y al finalizar el trabajo cerrarla
 * @author JuanBG
 *
 */
@Repository
public class UserDAO implements CrudImpl<User> {

	@Autowired
	SessionFactory sessionFactory; //Referencia Singleton a el helper de hibernate

	
	/**
	 * Método que inserta en la base de datos
	 * @param objeto a insertar
	 * @return estatus de operacón en formato string 
	 * @exception Si algo llega a fallar una exception será lanzada 
	 */
	@Override
	public String insertar(User t) throws SQLException {
		Session session = sessionFactory.openSession(); //abrir sesión
		try {
			session.beginTransaction(); // Comenzar transacción, todas las operaciones que tocan la base de datos, se les conoce como transacciones.
			session.save(t); //una vez abierta la sesión le decimos a hibernate que guarde en el área de espera el recurso a persistir 
			session.getTransaction().commit();//le damos la orden a la transacción de realizar el commit a la base de datos 

			return "Exito al insertar"; //retornamos estatus
		} catch (Exception e) {
			session.getTransaction().rollback(); //si algo falla hacemos un rollback para deshacer los posibles cambios
			e.printStackTrace();
			throw new SQLException("Existe un problema con la base de datos \n" + "Error al insertar.");
		} finally {
			session.close();//cerramos sesión

		}

	}

	/**
	 * método que realiza una actualización.
	 * @param Objeto que ya fue editado
	 * @return estatus de operacón en formato string 
	 * @exception Si algo llega a fallar una exception será lanzada 
	 * 
	 */
	@Override
	public String editar(User t) throws SQLException {
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction(); //comenzamos transacción  
			User r = session.get(User.class, t.getUsername()); //Conseguimos la referencia del objeto viejo de la base de datos
			r = t; //al objeto viejo, le sobre escribimos el nuevo objeto, cabe mencionar que con esto no perdemos el id, que es lo que hibernate necesita para persistir el objeto correcto
			session.merge(r); //se guardan los cambios en el stage area
			session.getTransaction().commit(); //se hace el commit
			return "Exito al Actualizar";
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			throw new SQLException("Error al editar: " + e.getMessage());
		} finally {
			session.close();
		}
	}

	/**
	 * Método que elimina
	 * @param id de usuario a eliminar
	 * @return estatus de operación
	 */
	@Override
	public String eliminar(int t) throws SQLException {
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			User user = session.get(User.class, t); //obtiene el objeto que será eliminado a través del id 
			session.delete(user); // eliminamos 
			session.getTransaction().commit();
			return "Exito al eliminar";
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			throw new SQLException("Error al eliminar: " + e.getMessage());
		} finally {
			session.close();
		}
	}

	/**
	 * @return lista de usuarios
	 */
	@Override
	public List<User> consultaGeneral() throws SQLException {
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			Query query = session.createQuery("from User"); //HQL que hace referencia a un SELECT * FROM users, sólo que esta hace una referencia al Objeto mapeado en el hibernate.cgf.xml.
			List<User> users = query.list(); //obtenemos una lista 
			session.getTransaction().commit();
			return users;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SQLException("Error al consultar: " + e.getMessage());
		} finally {
			session.close();
		}

	}

	/**
	 * consulta individual
	 * @param nombre de usuario
	 * @return usuario compelto
	 * 
	 */
	@Override
	public User consultaIndividual(String t) throws SQLException {
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			User user;
			Query query = session.createQuery("from User where username = :u_name"); // HQL que hace referencia a SELECT * from users 
			query.setString("u_name", t); // se le manda el parametro u_name
			user = (User) query.uniqueResult();
			session.getTransaction().commit();
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SQLException("Error al consultar: " + e.getMessage());
		} finally {
			session.close();
		}
	}

}
