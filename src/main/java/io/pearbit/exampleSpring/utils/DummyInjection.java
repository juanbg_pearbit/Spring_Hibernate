package io.pearbit.exampleSpring.utils;


import java.sql.SQLException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.pearbit.exampleSpring.config.SpringWebConfig;
import io.pearbit.exampleSpring.dao.sql.RolDAO;
import io.pearbit.exampleSpring.models.Rol;
/**
 * Class created for dummy purposes.
 * @author JuanBG
 *
 */
@Component
public class DummyInjection {
	
	@Autowired
	RolDAO dao;
	
	/**
	 * it will be executed after the project construction, after {@link SpringWebConfig}.
	 * @return
	 */
	@PostConstruct
	public boolean createRole(){
		
		Rol rol_adm = new Rol(0, "ADMIN");
		Rol rol_usr = new Rol(0, "USER");

		try {
			dao.insertar(rol_adm);
			dao.insertar(rol_usr);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return true;
	}
	
	
}
