package io.pearbit.exampleSpring.controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import io.pearbit.exampleSpring.dao.sql.RolDAO;
import io.pearbit.exampleSpring.dao.sql.UserDAO;
import io.pearbit.exampleSpring.models.Rol;
import io.pearbit.exampleSpring.models.User;
/**
 * Class which will be similar to a Servlet in J2EE.
 * @author JuanBG
 *
 */
@Controller //This annotation enable this class like a bean -> component of Spring MVC 
@RequestMapping({ "/", "/login" }) //url by which this will be called
public class LoginController {

	@Autowired //create automatic reference to UserDAO (UserDAO is another component annotated with Repository)
	UserDAO daoUser;
	@Autowired //create automatic reference to RolDAO (RolDAO is another component annotated with Repository)
	RolDAO daoRol;

	
	/**
	 * Method which shows the initial view, it has not path, it means it will response to his directly parent in this case ("/","/login")
	 * @return name of the view that it must to show
	 */
	@GetMapping
	public String showLoginView() {
		return "login";
	}

	/**
	 * Method which perform login action 
	 * @param username send in form 
	 * @param password send in form
	 * @return home view, showed to users with correct login 
	 */
	@PostMapping("/perform_login")
	public ModelAndView performLogin(@RequestParam("username") String username,
			@RequestParam("password") String password) {

		ModelAndView model = new ModelAndView(); //New Model and view

		User user = null; //new user container
		try {
			user = daoUser.consultaIndividual(username); // we make a new query to get a single user name, by username

			if (user != null) { //if the user recently get isn't null
				if (user.getPassword().equals(password)) { //verify if password, provided and stored are equals
					model.setViewName("home"); //put view name, in this case home

				} else {
					model.setViewName("login"); //in error return to the same page, login, but with a error message
					model.addObject("error", "Invalid username and password!");

				}
			} else {
				model.setViewName("login"); //in error return to the same page, login, but with a error message
				model.addObject("error", "User not exist!");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.setViewName("login");
			model.addObject("error", e.getMessage());
		}
		return model;
	}

	/**
	 * Method to show sign up page
	 * @param msg 
	 * @return return page to sign up
	 */
	@GetMapping("/reg")
	public ModelAndView showRegView(String msg) {
		ModelAndView model = new ModelAndView();
		if (msg == null) { //if msg is empty means that is called by browser
			try {
				List<Rol> roles = daoRol.consultaGeneral(); //get all roles
				if (roles != null) { 
					model.setViewName("register"); //we show the view named register
					model.addObject("roles", roles); //we inject a collection of all roles that we get 
				} else {
					model.setViewName("register"); //something wrong! 
					model.addObject("error", "No roles found");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				model.setViewName("login"); 
				model.addObject("error", e.getMessage());
			}
		} else {
			model.setViewName("register");
			model.addObject("error", msg);
		}
		return model;
	}

	/**
	 * Method to perform a new sign up
	 * @param username send by html form
	 * @param password send by html form
	 * @param repassword send by html form
	 * @param rol send by html form
	 * @return return to login view in success case
	 * 
	 * !WARNING: This method is for dummy and demo purposes it only allows one role for each user, 
	 * in the really word, the user must to be enable to select all roles that he wants.¡
	 */
	@PostMapping("/reg")
	public ModelAndView performRegister(@RequestParam("username") String username,
			@RequestParam("password") String password, @RequestParam("repassword") String repassword,
			@RequestParam("rol") String rol) {


		ModelAndView model = new ModelAndView();

		List<Rol> roles = new ArrayList<>();
		roles.add(new Rol(0, rol));

		try {
			if (password.equals(repassword)) {
				String conf = daoUser.insertar(new User(0, username, password, 1, roles));

				if (conf != "" || conf != null) {
					model.setViewName("login");
					model.addObject("msg", "User created! ");
				}
			} else {
				showRegView("Passwords not equals");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
			model.setViewName("login");
			model.addObject("error", "User created! ");
		}

		return model;

	}
}
