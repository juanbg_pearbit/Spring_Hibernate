<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"  isELIgnored="false"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
			<!DOCTYPE html>
			<html>

			<head>
				<meta charset="UTF-8">
				<title>Material Design Login Form</title>



				<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/style.css">


			</head>

			<body>
				<hgroup>
					<h1>Material Design Form</h1>
					<c:if test="${not empty error}">
						<div class="error">
							<p class="text-center">${error}</p>
						</div>
					</c:if>
					<c:if test="${not empty msg}">
						<div class="msg">
							<p class="text-center">${msg}</p>
						</div>
					</c:if>
				</hgroup>
				<form name='f' class="login_form" action="perform_login" method='POST'>
					<div class="group">
						<input type="text" name='username'>
						<span class="highlight"></span>
						<span class="bar"></span>
						<label>Username</label>
					</div>
					<div class="group">
						<input type="password" name='password'>
						<span class="highlight"></span>
						<span class="bar"></span>
						<label>Password</label>
					</div>
					<div class="group">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					</div>
					<div class="group">
					<center><a href="<%=request.getContextPath()%>/reg">No user yet? Click Here!</a></center>
					</div>
					<input type="submit" value="Log In" class="button buttonBlue">
				</form>
				<footer>
					<a href="http://www.polymer-project.org/" target="_blank">
						<img src="https://www.polymer-project.org/images/logos/p-logo.svg">
					</a>
					<p>
						You Gotta Love
						<a href="http://www.polymer-project.org/" target="_blank">Google</a>
					</p>
				</footer>
				<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

				<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/index.js"></script>

			</body>

			</html>